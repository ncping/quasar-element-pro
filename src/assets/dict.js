const sysStatus = {
  '0': {
    'label': '正常',
    'color': 'positive'
  },
  '2': {
    'label': '停用',
    'color': 'negative'
  }
}
const policyType = {
  '1': '系统策略',
  '2': '自定义策略'
}

module.exports = {
  sysStatus,
  policyType
}
