
const routes = [
  {
    path: '/user',
    component: () => import('layouts/UserLayout.vue'),
    children: [
      { path: '', redirect: { name: 'user-login' } },
      { path: 'login', name: 'user-login', component: () => import('pages/Login.vue') },
      { path: 'signup', name: 'user-signup', component: () => import('pages/Signup.vue') }
    ]
  },
  {
    path: '/503',
    component: () => import('pages/Error503.vue')
  },
  {
    path: '/404',
    component: () => import('pages/Error404.vue')
  },
  {
    path: '/',
    component: () => import('layouts/MainLayoutNew.vue'),
    // meta: { auth: true },
    children: [
      { path: '', redirect: { path: 'dashboard' } },
      { path: 'dashboard', meta: { auth: true }, component: () => import('pages/Dashboard.vue') },
      {
        path: 'component',
        component: () => import('layouts/BlankLayout.vue'),
        children: [
          { path: '', redirect: { path: 'overview' } },
          { path: 'overview', meta: { sidebar: true, auth: true }, component: () => import('pages/system/Overview.vue') },
          {
            path: 'form',
            component: () => import('layouts/BlankLayout.vue'),
            children: [
              { path: '', redirect: { path: 'basic-form' } },
              { path: 'basic-form', meta: { sidebar: true, auth: true }, component: () => import('pages/form/BasicForm.vue') },
              { path: 'step-form', meta: { sidebar: true, auth: true }, component: () => import('pages/form/StepForm.vue') }
            ]
          },
          {
            path: 'list',
            component: () => import('layouts/BlankLayout.vue'),
            children: [
              { path: '', redirect: { path: 'basic-list' } },
              { path: 'table-list', meta: { sidebar: true, auth: true }, component: () => import('pages/list/TableList.vue') },
              { path: 'basic-list', meta: { sidebar: true, auth: true }, component: () => import('pages/list/BasicList.vue') }
            ]
          }
        ]
      },
      {
        path: 'system',
        component: () => import('layouts/BlankLayout.vue'),
        children: [
          { path: '', redirect: { path: 'admin/users' } },
          { path: 'admin', redirect: { path: 'admin/users' } },
          { path: 'admin/users', meta: { sidebar: true, auth: true }, component: () => import('pages/system/UserList.vue') },
          { path: 'admin/users/log', meta: { sidebar: true, auth: true }, component: () => import('pages/system/LogList.vue') },
          { path: 'admin/users/:username', meta: { sidebar: true, auth: true }, component: () => import('pages/system/UserDetail.vue') },
          { path: 'admin/roles', meta: { sidebar: true, auth: true }, component: () => import('pages/system/RoleList.vue') },
          { path: 'admin/roles/menu/:id', meta: { sidebar: true, auth: true }, component: () => import('pages/system/RoleMenuEdit.vue') },
          { path: 'admin/policies', meta: { sidebar: true, auth: true }, component: () => import('pages/system/PolicyList.vue') },
          { path: 'admin/policies/:id', meta: { sidebar: true, auth: true }, component: () => import('pages/system/PolicyEdit.vue') },
          { path: 'metrics', meta: { sidebar: true, auth: true }, component: () => import('pages/system/Metrics.vue') },
          { path: 'menus', meta: { sidebar: true, auth: true }, component: () => import('pages/system/MenuList.vue') }
        ]
      },
      {
        path: 'goods',
        component: () => import('layouts/BlankLayout.vue'),
        children: [
          { path: '', redirect: { path: 'brand' } },
          { path: 'brand', meta: { sidebar: true, auth: true }, component: () => import('pages/shop/goods/BrandList.vue') }
        ]
      },
      {
        path: 'order',
        component: () => import('layouts/BlankLayout.vue'),
        children: [
          { path: '', meta: { sidebar: true, auth: true }, component: () => import('pages/Error404.vue') }
        ]
      },
      {
        path: 'account',
        component: () => import('layouts/BlankLayout.vue'),
        children: [
          { path: '', meta: { sidebar: true, auth: true }, component: () => import('pages/Error404.vue') }
        ]
      },
      {
        path: 'menuTree',
        component: () => import('pages/system/MenuTree.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    redirect: { path: '/404' }
  })
}

export default routes
